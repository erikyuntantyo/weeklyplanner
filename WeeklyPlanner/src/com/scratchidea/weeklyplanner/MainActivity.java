package com.scratchidea.weeklyplanner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import com.scratchidea.weeklyplanner.util.ExtendedFragmentStatePagerAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
    private ImageButton buttonNext;
    private ImageButton buttonPrevious;
    private TextView textViewDateRange;
    private ViewPager viewPager;

    private Calendar endDate;
    private Calendar startDate;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        startDate = Calendar.getInstance(Locale.getDefault());
        startDate.add(Calendar.DATE, -startDate.get(Calendar.DAY_OF_WEEK) + 1);
        
        endDate = Calendar.getInstance(Locale.getDefault());
        endDate.setTime(startDate.getTime());
        endDate.set(Calendar.DATE, startDate.get(Calendar.DATE) + 6);

        buttonNext = (ImageButton)findViewById(R.id.button_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDate.add(Calendar.WEEK_OF_MONTH, 1);
                startDate.add(Calendar.DATE, -startDate.get(Calendar.DAY_OF_WEEK) + 1);
                endDate.setTime(startDate.getTime());
                endDate.set(Calendar.DATE, startDate.get(Calendar.DATE) + 6);
                updateDateRange(startDate, endDate);
            }
        });
        
        buttonPrevious = (ImageButton)findViewById(R.id.button_previous);
        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDate.add(Calendar.WEEK_OF_MONTH, -1);
                startDate.add(Calendar.DATE, -startDate.get(Calendar.DAY_OF_WEEK) + 1);
                endDate.setTime(startDate.getTime());
                endDate.set(Calendar.DATE, startDate.get(Calendar.DATE) + 6);
                updateDateRange(startDate, endDate);
            }
        });
        
        textViewDateRange = (TextView)findViewById(R.id.text_view_date_range);
        
        updateDateRange(startDate, endDate);
        
        viewPager = (ViewPager)findViewById(R.id.view_pager);
        viewPager.setAdapter(new ExtendedFragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int arg0) {
                return new DynamicFragment(arg0);
            }
        });
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        
        if (id == R.id.action_settings) {
            return true;
        }
        
        return super.onOptionsItemSelected(item);
    }
    
    private void updateDateRange(Calendar startDate, Calendar endDate) {
        StringBuilder builder = new StringBuilder();
        Float textSize = 0f;
        
        if (endDate.get(Calendar.MONTH) == startDate.get(Calendar.MONTH)) {
            builder.append(new SimpleDateFormat("dd", Locale.getDefault()).format(startDate.getTime()))
                   .append(" - ")
                   .append(new SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).format(endDate.getTime()));
            
            textSize = 18f;
        } else {
            builder.append(new SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).format(startDate.getTime()))
                   .append(" - ")
                   .append(new SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).format(endDate.getTime()));
            textSize = 16f;
        }
        
        textViewDateRange.setText(builder.toString());
        textViewDateRange.setTextSize(textSize);
    }
}
