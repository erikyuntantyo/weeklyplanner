package com.scratchidea.weeklyplanner.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

/**
 * The extended class of FragmentStatePagerAdapter that to save
 * registered fragments to memory.
 *
 * @author Erik P. Yuntantyo
 */
public abstract class ExtendedFragmentStatePagerAdapter
    extends FragmentStatePagerAdapter {
    private SparseArray<Fragment> registeredFragments;

    public ExtendedFragmentStatePagerAdapter(FragmentManager fm) {
        super(fm);
        registeredFragments = new SparseArray<Fragment>();
    }

    @Override
    public int getCount() {
        return 3;
    }
    
    @Override
    public Object instantiateItem(ViewGroup arg0, int arg1) {
        Log.d("EXTENDED_FRAGMENT", "Fragment Index: " + arg1);
        Fragment fragment = (Fragment)super.instantiateItem(arg0, arg1);
        registeredFragments.append(arg1, fragment);
        return fragment;
    }
    
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }
    
    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}
