package com.scratchidea.weeklyplanner;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * The fragment contained dynamic contents.
 *
 * @author Erik P. Yuntantyo
 */
public class DynamicFragment extends Fragment {
    private final String WEEK_OF_MONTH_KEY = "week_of_month";
    private int weekOfMonth;
    
    public DynamicFragment(int weekOfMonth) {
        Bundle args = new Bundle();
        args.putInt(WEEK_OF_MONTH_KEY, weekOfMonth);
        setArguments(args);
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        weekOfMonth = getArguments().getInt(WEEK_OF_MONTH_KEY);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dynamic, container, false);
        TextView textView = (TextView)view.findViewById(R.id.text_view);
        
        textView.setText(getDateRange(weekOfMonth));
        
        return view;
    }
    
    private String getDateRange(int weekOfMonth) {
        return "Page#" + weekOfMonth;
    }
}
